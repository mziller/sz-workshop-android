package com.example.sz.workshop.auth;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.util.Log;

public class GoogleAuth extends Activity{

	public String[] getAccountNames() {
		 AccountManager mAccountManager = AccountManager.get(this);
	    Account[] accounts = mAccountManager.getAccountsByType(
	            GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
	    String[] names = new String[accounts.length];
	    for (int i = 0; i < names.length; i++) {
	        names[i] = accounts[i].name;
	    }
	    return names;
	}
	
	public void obtainToken(){
		Activity mActivity;
		String mEmail;
		String mScope;
		String token;

		try {
		    token = GoogleAuthUtil.getToken(mActivity, mEmail, mScope);
		} catch(Exception e) {
			//
		}
	}
	
	public void useToken(Activity mActivity, String token){
		URL url = new URL("https://www.googleapis.com/oauth2/v1/userinfo?access_token="
		        + token);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		int serverCode = con.getResponseCode();
		//successful query
		if (serverCode == 200) {
		    InputStream is = con.getInputStream();
		    String name = getFirstName(readResponse(is));
		    mActivity.show("Hello " + name + "!");
		    is.close();
		    return;
		//bad token, invalidate and get a new one
		} else if (serverCode == 401) {
		    GoogleAuthUtil.invalidateToken(mActivity, token);
		    onError("Server auth error, please try again.", null);
		    Log.e(TAG, "Server auth error: " + readResponse(con.getErrorStream()));
		    return;
		//unknown error, do something else
		} else {
		    Log.e("Server returned the following error code: " + serverCode, null);
		    return;
		}
	}
}
