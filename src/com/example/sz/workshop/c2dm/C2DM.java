package com.example.sz.workshop.c2dm;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;

/**
 *(De)-Register for the C2DM service. Push Events are published by a server
 * @author mazi
 *
 */
public class C2DM extends Activity {
	
	public void register(){
		Intent registrationIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
		 registrationIntent.setPackage("com.google.android.gsf");
		         registrationIntent.putExtra("app", 
		             PendingIntent.getBroadcast(this, 0, new Intent(), 0)); 
		 registrationIntent.putExtra("sender", "SUEDDEUTSCHE@ZEITUNG.de");
		 startService(registrationIntent);
	}
	
	public void unregister(){
		Intent unregIntent = new Intent("com.google.android.c2dm.intent.UNREGISTER");
		 unregIntent.putExtra("app", 
		                 PendingIntent.getBroadcast(this, 0, new Intent(), 0));
		 startService(unregIntent);
	}

}
